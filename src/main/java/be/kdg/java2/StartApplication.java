package be.kdg.java2;

import java.util.ArrayList;
import java.util.List;

public class StartApplication {
    //Write a function that has a lambda as a parameter
    //the lambda has an int input and a double output
    public static void calculator(MyFunctionalInterface theLambda) {
        System.out.println("Calculating...");
        for (int i = 0; i < 10; i++) {
            System.out.println(theLambda.myMethod(i));
        }
    }

    public static void main(String[] args) {
        calculator(n -> Math.pow(10, n));
        calculator(n -> {
            double result = 1;
            for (int i = 1; i <= n; i++) result *= i;
            return result;
        });
        //make a list of 10 lambda's
        List<MyFunctionalInterface> lambdas = new ArrayList<>();
        for (int i=0;i<10;i++) {
            final int j = i;
            lambdas.add(n->Math.pow(j, n));
        }
        //pass all these lambda's to the calculator function
        lambdas.forEach(l->calculator(l));
    }
}
