package be.kdg.java2;

@FunctionalInterface
public interface MyFunctionalInterface {
    double myMethod(int myParam);
}
